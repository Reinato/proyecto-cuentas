export class ItemCuenta {
  descripcion: string;
  monto: number;

  constructor(d: string, m: number) {
    this.descripcion = d;
    this.monto = m;
  }

}
