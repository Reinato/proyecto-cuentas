import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaEgresosComponent } from './lista-egresos.component';

describe('ListaEgresosComponent', () => {
  let component: ListaEgresosComponent;
  let fixture: ComponentFixture<ListaEgresosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaEgresosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaEgresosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
