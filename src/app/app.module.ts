import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListaIngresosComponent } from './lista-ingresos/lista-ingresos.component';
import { ListaEgresosComponent } from './lista-egresos/lista-egresos.component';
import { ItemCuentaComponent } from './item-cuenta/item-cuenta.component';
import { FormularioComponent } from './formulario/formulario.component';

@NgModule({
  declarations: [
    AppComponent,
    ListaIngresosComponent,
    ListaEgresosComponent,
    ItemCuentaComponent,
    FormularioComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
