import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemCuentaComponent } from './item-cuenta.component';

describe('ItemCuentaComponent', () => {
  let component: ItemCuentaComponent;
  let fixture: ComponentFixture<ItemCuentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemCuentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemCuentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
