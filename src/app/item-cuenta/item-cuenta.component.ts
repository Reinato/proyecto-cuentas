import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { ItemCuenta } from '../models/item-cuenta.model';

@Component({
  selector: 'app-item-cuenta',
  templateUrl: './item-cuenta.component.html',
  styleUrls: ['./item-cuenta.component.css']
})
export class ItemCuentaComponent implements OnInit {
  @Input() itemCuenta: ItemCuenta;
  @HostBinding('attr.class') cssClass = 'col-md-4';

  constructor() { }

  ngOnInit(): void {
  }

}
