import { Component, OnInit } from '@angular/core';
import { ItemCuenta } from '../models/item-cuenta.model';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
itemsCuenta: ItemCuenta[];
  constructor() {
    this.itemsCuenta = [];
  }

  ngOnInit(): void {
  }

  agregarItem(descripcion: string, monto: string): boolean {
    this.itemsCuenta.push(new ItemCuenta(descripcion, Number(monto)));

    return false;
  }

}
